# qBittorrent Telegram Bot
# Copyright (C) 2016-2017, Filippo Rigotto.
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of QbtBot nor the names of its contributors
#       may be used to endorse or promote products derived from this software
#       without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL Filippo Rigotto BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Crontab: @reboot sleep 30; su (user) -c 'python (dir)/qbbot.py >> (dir)/qbbot.log 2&>1 &'
# Dependencies: telepot, python-qbittorrent
import datetime, qbittorrent, signal, sys, time, telepot

# EDIT these values
myId = 000
botToken = '000'
qbtUrl  = 'http://localhost:8080'
qbtUser = '000'
qbtPass = '000'

startStr = 'This Bot can control an instance of qBittorrent.\nNot publicly available.'
cmdStr = 'List of available commands:\n \
/ver - Get qbt version\n \
/list - Get torrent list\n \
/add <magnet> - Download from magnet\n \
/pause - Pause all torrents\n \
/pause <hash> - Pause torrent\n \
/resume - Resume all torrents\n \
/resume <hash> - Resume torrent\n \
/delete <hash> - Delete torrent\n \
/close - Shutdown qbt\n \
/id - Get user\'s id'
logStr = 'Bot Log Legenda:\n \
L - General events\n \
R - Msg rcvd\n \
S - Msg sent\n \
E - Msg sending error'

def trySend(id,msg):    #wrapping for logging
    try:
        print "S | {0} | {1}".format(id,msg.replace('\n',' ')[:20])
        bot.sendMessage(id,msg)
    except Exception as exc:
        print "E | {0} | {1} | {2}".format(id,msg[:20],exc)

def handle(msg):
    chatId = msg['chat']['id']
    text = msg['text'].strip()

    # DECODE COMMANDS

    cmd = ''
    arg = ''
    if text.startswith('/') and ' ' in text:
        spl = text.split(' ',1)
        cmd = spl[0]    #/cmd arg...
        arg = spl[1]
    elif text.startswith('/'):
        cmd = text      #/cmd

    # EXECUTE COMMANDS

    global qb

    if cmd == '/start':
    	trySend(chatId,startStr)
    elif cmd == '/help':
    	trySend(chatId,cmdStr)

    elif cmd == '/close'   and chatId == myId and qb != None:
        trySend(myId,'Shutting down...')
        qb.shutdown()
        qb = None

    elif cmd == '/ver'    and chatId == myId and qb != None:
        trySend(myId,"Version: {0}".format(qb.qbittorrent_version))

    elif cmd == '/list'   and chatId == myId and qb != None:
        tntlist = qb.torrents(status='all',sort='progress',limit=0)
        if len(tntlist) > 0:
            res = ''
            for tnt in tntlist:
                res += "Name: {0}\nState: {1}\nSpeed: {2:.1f}KB/s\nProgress: {3:.2f}%\nHash: {4}\n\n".format(tnt['name'],tnt['state'],tnt['dlspeed']/1024,tnt['progress']*100,tnt['hash'])
            trySend(myId,res)

    elif cmd == '/add'    and chatId == myId and qb != None:
        if arg != '':
            arr = text.split(' ',1)
            trySend(myId,'Adding {0}'.format(arr[1]))
            qb.download_from_link(arr[1])
        else:
            trySend(myId,'No magnet to use!')

    elif cmd == '/pause'  and chatId == myId and qb != None:
        if arg == '':
            trySend(myId,'Pausing all torrents...')
            qb.pause_all()
        else:
            arr = text.split(' ',1)
            trySend(myId,'Pausing {0}'.format(arr[1]))
            qb.pause(arr[1])

    elif cmd == '/resume' and chatId == myId and qb != None:
        if arg == '':
            trySend(myId,'Resuming all torrents...')
            qb.resume_all()
        else:
            arr = text.split(' ',1)
            trySend(myId,'Resuming {0}'.format(arr[1]))
            qb.resume(arr[1])

    elif cmd == '/delete' and chatId == myId and qb != None:
        if arg != '':
            arr = text.split(' ',1)
            trySend(myId,'Deleting {0}'.format(arr[1]))
            qb.delete([arr[1]])
        else:
            trySend(myId,'No torrent hash to use!')

    elif cmd == '/id':
    	trySend(chatId,"Id = {0}".format(chatId))

def sigHdlr(sig,frame): #notify, log and safely shut down script
    print "L | {0} Stopped!\n\n".format(str(datetime.datetime.now()).split('.')[0])
    sys.exit(0)

# MAIN PROGRAM

#linking bot
#attempt to initialize w/o network errors
bot = None
while bot == None:
    try:
        bot = telepot.Bot(botToken)
        print "L | {0} Bot connected!".format(str(datetime.datetime.now()).split('.')[0])
    except:
        bot = None
        time.sleep(5)

#linking qbittorrent
qb = None
qbNum = 0
while qb == None and qbNum < 5:
    try:
        qb = qbittorrent.Client(qbtUrl)
        qb.login(qbtUser,qbtPass)
        print "L | {0} Qbt connected!".format(str(datetime.datetime.now()).split('.')[0])
    except:
        qbNum = qbNum + 1
        qb = None
        print "L | {0} Qbt failed!".format(str(datetime.datetime.now()).split('.')[0])
        time.sleep(2)

#exit after 5 failed connections
if qb == None:
    print "L | {0} EXITING!".format(str(datetime.datetime.now()).split('.')[0])
    sys.exit(1)

print logStr

for sig in [signal.SIGINT,signal.SIGHUP,signal.SIGQUIT,signal.SIGTERM]:
    signal.signal(sig,sigHdlr)  #attaching signals

bot.message_loop(handle, run_forever=True)
#calls getUpdate every 0.1 seconds and msg handling if necessary
